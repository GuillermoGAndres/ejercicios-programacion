/**
 * Elabora un programa calcule el perímetro de un Triangulo isoceles. El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>


int main(void){
    float lado,base;
    printf("Perimetro de de una triangulo isoceles\n" );
    printf("Escriba la longitud de un lado: ");
    scanf("%f", &lado);
    printf("Escriba su base");
    scanf("%f", &base);
    printf("El perimetro de su triangulo isoceles es: %.2f", lado*2 + base );
    return 0;
}
    

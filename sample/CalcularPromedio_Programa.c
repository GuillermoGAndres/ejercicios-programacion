#include <stdio.h>

int main()
{
    double calificacion1, calificacion2, calificacion3, promedio;
    char mensaje_error[] = "La calificación debe ser mayor o igual a 0 y menor o igual a 10";
    
    printf("Ingresa calificacion 1: ");
    scanf("%lf", &calificacion1);
    if ( calificacion1 >= 0 && calificacion1 <= 10 ) {
        printf("\nIngresa calificacion 2: ");
        scanf("%lf", &calificacion2);
        if ( calificacion2 >= 0 && calificacion2 <= 10 ) {
            printf("\nIngresa calificacion 3: ");
            scanf("%lf", &calificacion3);
            if ( calificacion3 >= 0 && calificacion3 <= 10 ) {
                promedio = (calificacion1 + calificacion2 + calificacion3) / 3;
                printf("Promedio: %.1lf", promedio);
            } else {
                printf("%s", mensaje_error);
            }
        } else {
            printf("%s", mensaje_error);
        }
    } else {
        printf("%s", mensaje_error);
    }
    return 0;
}

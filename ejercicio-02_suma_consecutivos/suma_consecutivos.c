#include <stdio.h>
/**
 * Elabora un programa que reciba un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.
 * Contrains 1 < n < 50; 
 *
 */

int main(){
    int n,i,count=0;
    scanf("%d", &n);
    for(i=1; i <= n; i++){
        count += i;
    }
    printf("%d", count);

    return 0;
}

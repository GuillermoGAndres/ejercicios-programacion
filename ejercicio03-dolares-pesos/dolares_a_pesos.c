#include<stdio.h>
/**
 * Elabora un programa que reciba como entrada un monto en dólares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
 * */

int main(){
    float dolares,pesos_mexicanos=21.07, result;
    scanf("%f", &dolares);
    result = dolares * pesos_mexicanos;
    printf("%.2f", result);
    return 0;
}


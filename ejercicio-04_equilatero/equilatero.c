/**
 * Elabora un programa calcule el perímetro de un triángulo equilátero. El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>


int main(void){
    float lado;
    printf("Perimetro de de una triangulo quilatero\n" );
    printf("Escriba la longitud de un lado: ");
    scanf("%f", &lado);
    printf("El perimetro de su triangulo equilatero es: %.2f", lado*3 );
    return 0;
}
    

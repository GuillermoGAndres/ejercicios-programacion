/**
 * Elabora un programa calcule el perímetro de un Triangulo escaleno. El programa pedirá al usuario las entradas necesarias.
 */
#include <stdio.h>


int main(void){
    float lado,lado2,lado3;
    printf("Perimetro de de una triangulo escaleno\n" );
    printf("Escriba la longitud del primer lado: ");
    scanf("%f", &lado);
    printf("Escriba la longitud del segundo lado: ")
    scanf("%f", &lado2);
    printf("Escriba la longitud del tercer lado: ");
    scanf("%f", &lado3);
    printf("El perimetro de su triangulo escaleno es: %.2f", lado+lado2+lado3 );
    return 0;
}
    
